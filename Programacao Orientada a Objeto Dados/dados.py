import pandas as pd
import numpy as np

// construa um leitor de dados para excel
pd.read_excel('dados.xlsx')
// mostre os dados em uma tabela
pd.read_excel('dados.xlsx').head()
// mostre quais colunas existem no arquivo
pd.read_excel('dados.xlsx').columns
// mostre e divida os dados em grupos
pd.read_excel('dados.xlsx').groupby('coluna').sum()



paises = pd.Series(['Brasil', 'Alemanha', 'EUA'])
paises